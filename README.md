Cours_Web_LGS2
=============

Dans ce repositoire, vous trouverez tout le materiel suplémentaire qui vous permettera de bien suivre le cours de programmation Web (Résumés des cours, code d'exemple, devoirs de chaque semaine).

  

Week 1  
--------

__Quick Recap__: On a introduit les bases théoriques derrière le fonctionnement d'un site web et d'un browser, on a aussi définis les languages utilisés côté client et côté serveur.  
On a suivi par introduire la syntax de base d'HTML et on a terminé avec une introduction rapide au CSS. On a aussi parlé d'un éditeur de code nommé Sublime-text3.  

__Script du cours__: Vous trouverez un script détaillé du cours en allant à la page **[Week 1](https://gitlab.com/Dobios/cours_web_lgs2/blob/master/Week1/Week1.md)** du repositoire.

__Devoirs pour Week 2__:  
	1. Lire le script de Week 1  
	2. Créer un site web (style blog) en HTML-CSS utilisant tous les éléments mentionnés en cours:  
		- Une entête ``<h1>...</h1> ... <h5>...</h5>``  
		- Un paragraphe ``<p>...</p>``  
		- Du CSS ``<style>...</style>``  
		- Une image ``<img src="https://www.ex.com/image">``  

Week 2
--------

__Quick Recap__: On a continué notre découverte de l'HTML en y ajoutant les images, les liens clicables, les listes et finalement les boutons. Ceci nous a permis de créer des sites un peu plus intéressants à plusieurs pages!  
Les boutons nous ont aussi permis d'ouvrir nos yeux sur un autre élément important: le *JavaScrpit*, qui nous permet de rendre nos sites **Interactifs**!   
  
__Script du cours__: Vous trouverez un script détaillé du cours en allant à la page **[Week 2](https://gitlab.com/Dobios/cours_web_lgs2/blob/master/Week_2/Week2.md)** du repositoire.

__Devoirs pour Week 3__:  
1. Lire le script de Week 2.  
2. Créer un site web avec ***minimum*** 2 pages qui contient:   
		- Un boutton qui modifie une partie de la page.  
		- Deux fonctions en JS qui calculent (a + b)^2 et (a)^2 (donc le carré d'un nombre et l'identité remarquable (a+b)^2 = (a^2 + 2ab + b^2).  
		- Vous inspirer de mon code d'[exemple](https://gitlab.com/Dobios/cours_web_lgs2/blob/master/Week_2/page2.html) pour prendre l'input de l'utilisateur et l'utiliser dans vos fonctions.

Week 3
--------

__Quick Recap__: Cette semaine, on a surtout fait un cours de révision sur le JS. Donc rien de vraiment nouveau.  
Pour plus de détails, allez voir le script de la [Week 2](https://gitlab.com/Dobios/cours_web_lgs2/blob/master/Week_2/Week2.md).  

Week 6
-------

__Quick Recap__: Cette semaine, on a vu les bases de comment travailler avec des listes en JS, comment faire de la récursion de base et comment faire des fonctions anonymes / callbacks.  Allez voir le script pour [Week_6](https://gitlab.com/Dobios/cours_web_lgs2/blob/master/Week_6/Week_6.md).   

Week 7 & 8
---------

__Quick Recap__: Durant ces deux semaines, nous avons développé notre première application 100% *in browser* en JS. Il s'agissait d'un jeu de *Tic Tac Toe*, avec le *front-end* en HTML/CSS et la logique entièrement en JS (sans framework). Pour plus d'information sur comment on a développé le jeu, allez voir le script [How to make Tictactoe in JS](https://gitlab.com/Dobios/cours_web_lgs2/blob/master/TicTacToeJS/howToTTT.md).   

