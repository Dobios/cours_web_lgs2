<?php

//Arrays
$tab = array('A', 'B', 'C');
$tab1 = ['A', 'B', 'C'];
$tabmap = [
	0 => 'A',
	1 => 'B', 
	2 => 'C'
];

echo "tab : " . $tab[1] . " tabmap : " . $tabmap[1] . "\n";

//Map
$map = [
		'user' => 'password',
		'andrew' => '123123coolcool'
		];

echo "password : " .  $map['andrew'] . "\n";

echo "Is a in tab ? " . in_array('A', $tab) . "\n";
echo "Is f in tab ? " . in_array('F', $tab) . "\n";

echo "Where Is a in tab ? " . array_search('A', $tab) . "\n";
echo "Where Is c in tab ? " . array_search('C', $tab) . "\n";
echo "Where Is a in tab ? " . array_search('A', $tab) . "\n";
echo "Where Is w in tab ? " . array_search('W', $tab) . "\n";

function list_toString($array) {
	$result = "[";
	for ($i = 0; $i < sizeof($array) ; $i++) {
		if($i != sizeof($array) - 1) {
			$result .= $array[$i] . ", ";
		} else {
			$result .= $array[$i];
		}
	}
	$result .= "]";
	return $result;
}

function list_foreach($array) {
	$result = "[ ";
	foreach ($array as $val) {
		$result .= $val . " ";
	}
	$result .= "]";
	return $result;
}

function map_foreach($map) {
	$result = "[ ";
	foreach ($map as $key => $val) {
		$result .= $key . " => " . $val . " ";
	}
	$result .= "]";
	return $result;
}

echo map_foreach($map);

