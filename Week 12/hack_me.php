 <?php
    function secure_input($data) {
        $data = trim($data);                //removes all unnecessary spaces, tabs, newlines, ...
        $data = stripslashes($data);        //removes all backslashes (no more closing an html element)
        $data = htmlspecialchars($data);    //turns all html characters into elements (ex: '<' => '&lt')
        return $data;
    }

 ?>


 <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8"/>
        <title>Welcome</title>
    </head>

    <body onload="loadSecret()">
        <h1> Welcome to our site !</h1>
        <p>Your current Feedback: <br/>
            <?php if(!empty($_GET["feedback"])) {
                echo "Your comment: " . secure_input($_GET["feedback"]) . "<br />";
            }
            ?>
        </p>
        <?php echo "<img src=\"" . $_GET["dog"] . ".jpg\">"; ?>
    
        <script src="secret_script.js"></script>
    </body>
    </html>