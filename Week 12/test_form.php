<?php
    //Define the length of a day
    const DAY = 86400;
    
    $value = "";
    $password = "";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (empty($_POST["name"])) {
            $value = "default";
        } else {
            $value = $_POST["name"];
        }
        if(empty($_POST["password"])) {
            $password = "default";
        } else {
            $password = $_POST["password"];
        }

    }

    function secure_input($data) {
        $data = trim($data);                //removes all unnecessary spaces, tabs, newlines, ...
        $data = stripslashes($data);        //removes all backslashes (no more closing an html element)
        $data = htmlspecialchars($data);    //turns all html characters into elements (ex: '<' => '&lt')
        return $data;
    }

    //Save user name and psw to cookie 
    setcookie($value, $password, time() + (DAY), "/");

?>
    <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8"/>
        <title>Welcome</title>
    </head>

    <body>
        <h1> Welcome to our site !</h1>
        <p>Your current user: <br/>
            Name: <?php echo $value; ?> <br />
            <?php if(!empty($_POST["comment"])) {
                echo "Your comment: " . secure_input($_POST["comment"]) . "<br />";
            }
            ?>
        </p>
    
        <script src="secret_script.js"></script>
    </body>
    </html>