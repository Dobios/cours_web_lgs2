<?php 
class View {
    private $data = array();
    private $render = FALSE;
    
    public function __construct($template) {
        try {
            $file = $_SERVER["DOCUMENT_ROOT"] . '/views/' . trim(strtolower($template)) . '.view.php';

            if(file_exists($file)) {
                $this->render = $file;
            } else {
                throw new Exception('File does not exist!');
            }
        } catch (Exception $e) {
            echo "ERROR";
        }
    }

    public function assign($variable, $value) {
        $this->data[$variable] = $value;
    }

    public function __destruct() {
        extract($this->data);
        include($this->render);
    }
}