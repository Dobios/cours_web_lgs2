const tictactoe = new TicTacToeGame();
tictactoe.start();

function TicTacToeGame() {
	const board = new Board();
	const humanPlayer = new HumanPlayer(board, true);
	const humanPlayer2 = new HumanPlayer(board, false);
	const computerPlayer = new ComputerPlayer(board);
	let turn = 0;
	let adv = false; //false: Human v Human, true: Human v Computer

	//Starts the game
	this.start = function() {

		//only take into account the children of a div
		const config = { childList: true }; 

		//Create an MutationObserver that listens to whenever someone takes a turn
		const observer = new MutationObserver(() => takeTurn());

		//Ask the observer to observe every position
		board.positions.forEach((el) => observer.observe(el, config));

		//Ask the first player to take a turn 
		takeTurn();
	}

	//EXPERIMENTAL: QUITE BUGGY
	this.restart = function() {
		board.positions.forEach(p => {
			p.innerText = '';
			//p.className = "winner";
		});
		turn = 0;
		board = new Board();

	};

	this.toggleAdv = function() {
		adv = !adv;
		document.getElementById("adv").innerText = adv ? "Human vs Computer" : "Human vs Human";
	}

	function takeTurn() {
		//Check to see if someone has won at the begining of each turn
		if(board.checkForWinner()) {
			return;
		}
		//Check who's turn is it and make them play
		if(turn % 2 == 0) {
			humanPlayer.takeTurn();
		} else {
			if(adv) {
				computerPlayer.takeTurn();
			} else {
				humanPlayer2.takeTurn();
			}
		}

		++turn;
	};
}

function Board() {
	//Selects all possible places where we can place a board and makes
	//an array out of it 
	this.positions = Array.from(document.querySelectorAll('.col'));

	//Checks to see if someone has won the game
	// Positions are: 
	//	0 1 2
	//  3 4 5 
	//  6 7 8
	this.checkForWinner = function() {
		//variable for wether or not we have found a winner
		let winner = false;
		//Array containing all possible winning combinations
		const winningCombinations = [
			[0, 1, 2], 
			[3, 4, 5], 
			[6, 7, 8],
			[0, 4, 8],
			[2, 4, 6],
			[0, 3, 6],
			[1, 4, 7],
			[2, 5, 8]
		];

		//Renaiming to remove the this
		const positions = this.positions;
		
		winningCombinations.forEach(winningCombo => {
			//position of the inner text
			const posInnerText0 = positions[winningCombo[0]].innerText;
			const posInnerText1 = positions[winningCombo[1]].innerText;
			const posInnerText2 = positions[winningCombo[2]].innerText;

			//Check to see if our entry matches one of the winning combos
			const isWinningCombo = posInnerText0 !== '' && 
					posInnerText1 === posInnerText2 &&
					posInnerText0 == posInnerText1;

			
			//Handle a win
			if(isWinningCombo) {
				winner = true;
				//Give the winners a winning css class
				winningCombo.forEach(index => {
					positions[index].className += ' winner';
				});
			}
		});
		
		return winner;
	};
}

function HumanPlayer(board, xo) {
	//allows us to select a pon (2 player mode)
	let pion = xo ? 'X' : 'O';

	this.takeTurn = function() {
		//add an event listener that waits for the 'click' event to then handle turnTaken()
		board.positions.forEach(el =>
			el.addEventListener('click', handleTurnTaken));
	}

	function handleTurnTaken(event) {
		//Check to see if we can actually play there (2 player mode)
		if(event.target.innerText !== '') {
			return;
		}

		//When the event is fired modify the inner Text of the div
		event.target.innerText = pion;

		//Remove the event listener so that we can't play 
		board.positions.forEach(el => el.removeEventListener('click', handleTurnTaken));
	};
}

function ComputerPlayer(board) {
	//Super dum and places character randomly
	this.takeTurn = function() {
		//Compute the available positions and store them in an array
		const availablePositions = board.positions.filter(p => p.innerText === '');
		//Choose a random position
		const move = Math.floor(Math.random() * availablePositions.length);

		//Make computer take a turn 
		availablePositions[move].innerText = 'O';
	};
}

