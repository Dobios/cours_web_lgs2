const tictactoe = new TicTacToeGame();
tictactoe.start();

function TicTacToeGame() {
    const board = new Board();
    const humanPlayer = new HumanPlayer(board, true);
    const humanPlayer1 = new HumanPlayer(board, false);
    //const computerPlayer = new ComputerPlayer(board);
    let turn = 0;

    this.start = function() {

        //only take into account the children of a div
        const config = { childList: true};

        //create a mutationObserver that listnes to whenever someone takes a turn
        const observer = new MutationObserver(() => takeTurn());

        //Ask the observer to observe every positions
        board.positions.forEach(position =>
         observer.observe(position, config));

         //Ask the first player to play
         takeTurn();
    };

    this.restart = function() {
        board.positions.forEach(p => p.innerText = '');
        turn = 0;
    }

    function takeTurn() {
      //Check to see if someone has won
      if(board.checkWinner()) {
        return;
      }
      //Decide who gets to play and make them play
      if(turn %  2 === 0) {
        humanPlayer.takeTurn();
        console.log("turn asked");
      } else {
        humanPlayer1.takeTurn();
      }
      ++turn;
    };
}

function Board() {
    this.positions = Array.from(document.querySelectorAll('.col'));

    this.checkWinner = function() {
        //Checks wether or not a winner has been found
        let winner = false;

        /**
        *   0 1 2
        *   3 4 5
        *   6 7 8
        */

        const winningCombinations = [
                [0, 1, 2], //top line
                [3, 4, 5], //middle line
                [6, 7, 8], //bottom line
                [0, 3, 6], //col left
                [1, 4, 7], //col middle
                [2, 5, 8], //col right
                [0, 4, 8], //diag topleft
                [2, 4, 6] //diag topright
        ];

        const positions = this.positions;

        winningCombinations.forEach(winningCombo => {
            //positions of the inner texts within each combo
            const posInnerText0 = positions[winningCombo[0]].innerText;
            const posInnerText1 = positions[winningCombo[1]].innerText;
            const posInnerText2 = positions[winningCombo[2]].innerText;

            //Check to see if the combination is valid
            const isWinner = posInnerText0 !== '' &&
                posInnerText0 === posInnerText1 &&
                posInnerText1 === posInnerText2;

            //Handle winner
            if(isWinner) {
                winner = true;
                //Give the winners a winning class
                winningCombo.forEach(index => {
                    positions[index].className += ' winner';
                })
            }
        })
        return winner;

    }
}

function HumanPlayer(board, xo) {
    let pion = xo ? 'X' : 'O';

    this.takeTurn = function() {
        //Add an eventListener that waits for a 'click' event to then handle a  taken turn
        board.positions.forEach(position => position.addEventListener('click', handleTurnTaken));
        console.log("Turn taken");
    };

    function handleTurnTaken(event) {
        //Make sure that we can play
        if(event.target.innerText !== '') {
            return;
        }
        
        //When the event is fired modify the inner text fo the div
        event.target.innerText = pion;

        //Remove the event listener so that we can't play
        board.positions.forEach(p => p.removeEventListener('click', handleTurnTaken));

    };
}

function ComputerPlayer(board) {
  this.takeTurn = function() {
      const availablePositions = board.positions.filter(p => p.innerText === '');

      //Check that the computer can play
      if(availablePositions.length === 0) {
            return;
      }

      //Choose a random position
      const move = Math.floor(Math.random() * availablePositions.length);

      //Play
      availablePositions[move].innerText = 'O';
      
  }
}





