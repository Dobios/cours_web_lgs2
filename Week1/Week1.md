Week 1
==========

__TLDR__: On a introduit les bases théoriques derrière le fonctionnement d'un site web et d'un browser, on a aussi définis les languages utilisés côté client et côté serveur.  
On a suivi par introduire la syntax de base d'HTML et on a terminé avec une introduction rapide au CSS.  

Script:
=========
## Introduction  

### Qu'est-ce qu'un site web?  
Cette question peut avoir plusieurs réponses, mais on peut simplement dire qu'un site web est un interaction entre un serveur (qui contient le code de **traitement d'informations** et un client qui contient le code **d'affichage de cette information**.    
> Ce style de site suit donc le modèle ***Client-Serveur***.  
>> _Note_: Ce n'est pas le seul modèle qui existe, il y en a un autre qui aussi très commun nommé *[Peer-to-Peer](https://fr.wikipedia.org/wiki/Pair_%C3%A0_pair)*  

### Pourquoi programmer pour le Web?  
1. C'est facile **==>** On peut très rapidement voir et tester ce qu'on fait dans un browser (comme *[firefox](https://www.mozilla.org/en-US/firefox/new/)*)  
2. C'est efficace **==>** On voit assez souvent tout de suite d'où l'erreur vient.  
3. On peut "tout" faire  **==>** Un serveur est capable de supporter le même style de programme que votre ordinateur, donc il n'y a pas de raison qu'il ne soit pas non plus capable de faire la même chose.  

### Quel language?  
Alors cette question doit être répondue en deux temps, d'abord côté serveur et puis ensuite côté client.  
1. __Côté serveur__: PHP, C#, Java, Scala et peut-être JS (en gros tout ce qui est *[Orienté-Objet](https://fr.wikipedia.org/wiki/Programmation_orient%C3%A9e_objet)*)  
2. __Côté client__: HTML, CSS, JS  

## Linux  

### Qu'est-ce que c'est?  
Linux est un kernel libre de système d'exploitation basé sur Unix (comme MacOS).  
__Quoi?__ c'est en gros un *style* de Système d'exploitation.  
__Pourquoi?__: Linux permet à l'utilisateur d'avoir un contrôlé très poussé de son ordinateur et donc de pouvoir profiter un maximum de tous ce dont il est capable.  

### Comment utiliser un terminal?  
On utilise un terminal dans Linux avec le language *bash*:  
``cd <lieu>`` permet de se déplacer dans un dossier.    
``pwd`` permet de voir où on se trouve dans l'ordinateur.    
``ls`` permet de voir ce qui se trouve dans notre dossier actuel.   

``mkdir <nom_du_dossier>`` permet de créer un dossier.    
``touch <nom_du_fichier>.<extension_du_fichier>`` permet de créer un fichier.    
``cat <nom_du_fichier>`` permet de **lire** un fichier dans le terminal.   
  


HTML  
----
L'HTML (HyperText Markup Language) définis le squelette de votre site.
Ici vous trouverez une fiche de sytaxe rapide pour l'HTML.  
__Structure__: Tout code html doit se trouver encadré par la stucture suivante:
```
<!DOCTYPE html>  
	<html>  
	<head>  
		<title></title>  
	</head>  
	<body>  
	</body>  
	</html>
```  

Ici ``<html>...</html>`` représente tout le code qui sera lu comme de l'HTML.
``<title>...</title>`` représente le titre du site web (affiché dans l'onglet tout en haut du navigateur).  
``<head>...</head>`` représente la partie supérieure du site.   
``<body>...</body>``représent la partie principale du site.  

__Syntaxe__: À la suite, vous trouverez les quelques mots clefs HTML qu'on a vu pendant le cours:  
``<h1>...</h1>`` jusqu'à ``<h5>...</h5>`` définis un titre de tailles allant de 1 à 5 (1 étant le plus grand).   
``<p>...</p>`` définis un paragraphe où on peut écrire ce qu'on veut.  
``<br>`` définis un saut à la ligne et doit s'utiliser à l'interieur d'un paragraphe ou d'un titre.  
``<img src="#">`` définis une image où à la place du *#* il faut mettre le lien URL vers l'image en question (trouvée sur internet ou hébergée sur un serveur).  

## CSS 
Le CSS (Color Style Sheet) définis la *peau* de votre site (couleur, ombres, formats, polices, ...).  
Ici vous trouverez une fiche de sytaxe rapide pour le CSS.  
__Structure__: Le CSS peut être écrit de deux façons:  
1. *Inline* **==>** style définis directement dans l'objet HTML  
	``<h1 style="###">...</h1>``  
2. *Style Sheet* **==>** style définis dans une structure appart de l'HTML  
	``<style>...</style>``  

Durant le cours, on a surtout vu comment aligner et changer la couleur du texte:  
``<h1 style="text-align: center; color: red;">...</h1>``    

##### Syntaxe CSS
  
Pour pouvoir écrire du CSS dans une **Style sheet**, il faut d'abord définir des **classes**, ce qui va nous permettre de controler les blocs HTML à distance dans le CSS.  
``<h1 class="cool_title">...</h1>``   

Ensuite on peut acceder à cette classe en écrivant:  
``<style>  
	.cool_title {  
		text-align: center;  
		color: red;  
	}  
</style>``  

__Rules__: 
1. Chaque ligne doit se terminer par un ``;``  
2. On écrit tout le CSS de la façon suivante ``attribut: valeur;`` 

## Exemple  
Vous trouverez sur le repositoire un *[Exemple de code](https://gitlab.com/Dobios/cours_web_lgs2/blob/master/W1_example.html)*   

## Next Week
La semaine prochaine on verra encore d'autres éléments de l'HTML et surtout comment rendre un site interactif grâce au JavaScript !   
N'oubliez pas __Google est votre ami__ !  







