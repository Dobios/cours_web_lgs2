How to make a TicTacToe game in a browser using JavaScript
====================
### Brief
Pour développer ce jeu, nous allons suivre un style de programmation nommé *Programmation Orientée Objet* (OOP). Pour faire cela, il faut donc qu'on divise les fonctionnalités de notre jeu dans plusieurs objets (ou classes) qui ont chanqun un but précis. Ceci nous permettra de mieux modulariser notre jeu au cours du développement.  

Marche à Suivre
------- 
Le développement aura lieu en plusieurs étapes:   
1. Définir l'interface avec l'utilisateur (front-end).  
2. Définir la structure interne du jeu (OOP: **Game**, **Board**, **HumanPlayer**, *facultatif* **ComputerPlyer**).  
3. Relier l'interface graphique avec la logique du jeu (**MutationObserver**).  
4. Permettre aux joueurs de jouer (**takeTurn()**).  
5. Vérifier s'il y a un gagnant (**checkWin()**).  
6. *facultatif* Relancer une nouvelle partie (**restartGame()**).  

le Jeu
-------------
On a commencé, comme expliqué dans la marche à suivre, par définir l'interface graphique de notre jeu, qui sera faite entièrement en HTML/CSS.   

### Front-End
On va organiser notre front-end en créant une ***[matrice](https://fr.wikipedia.org/wiki/Matrice)*** de `<div>` imbriqués, ce qu'on va illustrer dans l'image suivante :