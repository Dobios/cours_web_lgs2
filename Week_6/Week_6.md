Script Rapide Week 6
================
_Brief_: On va rapidement résumer la syntaxe pour:   
1. Utiliser des listes en JS   
2. Utiliser des fonctions anonymes en JS   
3. Faire de la programmation orientée objet en JS   


Listes
--------

### À quoi ça sert?   
_Brief_: Les listes nous permettent d'organiser les données de façon un peu plus précise qu'avec des variables. On peut voir ça comme un regroupement de variables en une plus grande variable.   

### Syntaxe  

```javascript 
//instancier une liste
var list = [<val0>, <val1>, <val2>, ..., <val_n-1>];

//accèder à un élément de la liste 
var elem0 = list[0]

``` 
On peut aussi demander à la liste si elle contient un élément précis:   
```javascript
//vérifier si une liste contient un élément précis
var isInList = list.includes(<val1>); 

isInList == true //si la liste contient <val1>
isInList == false //si la liste ne contient pas <val1>
```   

Fonctions Anonymes (Lambdas)
-----------

_Brief_: Ils nous permettent d'enregistrer des fonctions dans des variables (utile pour l'Orienté Objet), sinon ça peut aussi être utile pour passer des fonctions en argument à une autre fonction.   

### Syntaxe  
```javascript
//Instantiate a class function named classFunction
var this.classFunction = function(arg1, agr2 ,...) { 
	//FUNCTION CODE 
}

//functionnal function call by giving a function another function
funcThatTakesAFunction(arg1, function {
	//Function code
}, arg3);

//callBack
(arg1, arg2) => {
	//Callback code 
}
```   

Pour l'instant on va supposer que les callbacks et les lambdas fonctionnent de la même façon.   

Programmation Orientée Objet (OOP)
-----------

_Brief_: Style de programmation qui nous permet d'organiser nos données comme on veut, en créant nos propres types nommés ***Objets***. Ces objets peuvent contenir des fonctions (nommées ***méthodes*** ou  ***class functions***) et des variables privées (nommées ***attributs***).   

### Syntaxe
Il y a 2 façons de définir des objets en JS:  
- Avec des ***fonctions objets***   
- Avec le mot clef ***class***  
On peut donner de l'information à l'objet venant du monde exterieur dans sa déclaration (ici *arg1* et *arg2*).  

```javascript
//Instantiate an object named Obj with a contructor that takes two attributes
function Obj(arg1, arg2) {
	//Object's attributes
	let attribute1 = arg1;
	let attribute2 = arg2;

	//method 1
	this.method1 = function(arg1, arg2, ...) {
		//method code
	}

	this.method2 = function() {
		//method code
	}

}

//Instantiate the same object using a class
class Obj {

	//special method that is called when creating the object
	constructor(arg1, arg2) {
		let attribute1 = arg1;
		let attribute2 = arg2;
	} 

	method1(arg1, arg2, ...) {
		//method code
	}

	method2() {
		//method code
	}
}
```  
Dans cet exemple les deux façon d'écrire sont identiques.   


