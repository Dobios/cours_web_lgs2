Script Week 2
===========
Brief
------
On a continué notre découverte de l'HTML en y ajoutant les images, les liens clicables, les listes et finalement les boutons. Ceci nous a permis de créer des sites un peu plus intéressants à plusieurs pages!  
Les boutons nous ont aussi permis d'ouvrir nos yeux sur un autre élément important: le *JavaScrpit*, qui nous permet de rendre nos sites **Interactifs**!  

Exemple 
-------
Vous trouverez dans le répositoire, un *[site exemplaire](https://gitlab.com/Dobios/cours_web_lgs2/tree/master/Week_2)* qui utilise tout les éléments vu pendant la semaine 2.  

Révisions
------
### Qu'a-t-on vu la dernière fois?
On a découvert comment créer des pages webs *statiques* grâce à l'__HTML__ et de les styliser grâce au __CSS__.  

### Les devoirs Week1
__Soucis__: Pas grand chose appart les fautes de syntaxe et l'indentation.  
Il faut absolument faire attention à l'*[indentation](https://courses.cs.washington.edu/courses/cse154/17au/styleguide/html-css/spacing-indentation-html.html)*, c'est cruciale pour pouvoir écrire un code ***compréhensible***.  

More HTML
-------
L'HTML nous permet de créer le squelette de notre site, c'est à dire tous les éléments de base qui vont constituer la structure interne du site.  
Par exemple, on ne *peut* pas utiliser l'HTML pour créer de l'interaction entre l'utilisateur et le client web, pour ça il faut utiliser du ***JS***.  
  
> Le *[JavaScript](https://www.computerhope.com/jargon/j/javascript.htm)* (aussi connu sous le nom de JS) est un language de *[Scripting](https://www.computerhope.com/jargon/s/script.htm)* et ne doit pas être confondu avec le *[Java](https://en.wikipedia.org/wiki/Java_(programming_language))* qui lui est un language *[Orienté-Objet](https://searchapparchitecture.techtarget.com/definition/object-oriented-programming-OOP)*.

### Images
Pour créer des images en HTML il faut utiliser l'élément ``<img>``.  
Cet élément peut avoir plusieurs *attributs*, les plus importants sont:  
```
<img src="insertImageURL" alt="insertAlternateText"> 
```
>  __src__ définis la source de l'image (donc il faut mettre le nom de l'image s'il se trouve dans le même dossier que le code du site).  
> __alt__ définis le texte alternatif, c'est-à-dire le texte qui s'affichera à la place de l'image si le browser n'arrive pas à la trouver (ex: si vous avez fait une faute de frappe dans le nom).
> > Ces deux mots clefs sont appelés des _attributs_ de l'image.  

### Liens 
Un lien peut nous permettre de quitter notre page actuelle pour aller vers une autre (soit de notre site, soit d'un site externe).  
Les liens (aussi connu sous le nom ***hyperlink***) sont définis par l'élément ``<a> ... </a>``.   
Cet élément peut avoir les attributs suivants:  
``<a href="InsertDestination" target="InsertOpenMethod"> ... </a>``
> __href__ définis l'endroit vers lequel la page va sauter si on clique sur le lien. Par exemple si je veux aller vers une autre page de mon site qui s'appelle "page2.html", alors j'aurait le lien suivant: ``<a href="page2.html">``   
> __taget__ définis la méthode d'ouverture (dans la page, dans un nouveau tab, ...). Par exemple si je veux que ma page s'ouvre dans un nouveau tab j'aurait le lien suivant: ``<a href="..." target="__blanc">`` 

### Listes 
Les listes peuvent être faites de 2 manières:  
	1. Liste non-ordonée (des points): ``<ul>  <li>...</li>  </ul>``   
	2. Liste ordonnée (des chiffres): ``<ol>  <li>...</li>  </ol>``  
	
### Boutons
Les bouttons servent à lancer une fonction *JavaScript* qui peut faire essentiellent ce qu'on veut. Un bouton s'écrit avec l'élément: ``<button> ... </button>``.   
Le bouton peut avoir un attribut:  ``<button onclick="JSFunction()"> ... </button>``.  
> __onclick__ permet de lancer une fonction JavaScript définis dans un script du site.


JavaScript
------
Le JavaScript nous permet d'interragir avec l'utilisateur. Ceci est fait grâce à des scripts introduits entre le ``</head>``et le ``<body>``de notre code HTML: ``<script> //JavaScript code </script>``.   

### Variables
Une variable permet d'enrgistrer une valeur qui peut être utilisée/modifiée plus tard dans le code: ``let <var_name> = <value>;``.  
Par exemple je peux créer une variable **x** qui prend la valeur 0: ``let x = 0;``et je peux la modifier plus tard: ``x = x + 1;``.

### Conditions et boucles 
Les condition sont définis par un bloc ``if(cond) { //code1 } else { //code2 }``.   
_Code1_ ne s'executera uniquement si la condition _cond_ est vraie (``cond == true``) si non le _Code2_ s'executera.  
  
Les boucles sont soit avec un index:  ``for(let i = 0; i < numberOfLoops; ++i) { ... }`` où là le code s'executera exactement numberOfLoops fois.  
Soit sans un index:  ``while(cond) { ... }`` où là code s'executera tant que ``cond == false``.

### Fonctions 
Pour *[modulariser](https://en.wikipedia.org/wiki/Modular_programming)* notre code, on peut utiliser des fonctions. Ceci nous permet de créer des _boites noires_ qui font une certaine action et qui peuvent être utilisés quand on le veut.   
On définis un fonction comme suit: ``function <name>(<attributes>) { //code }``.  
> Ici la fonction <name> peut être appelée en faisant ``<name>(...)``.
> > Par exemple si on a une fonction ``function square(x) { return x * x; }``, on peut l'appeler en faisant ``square(2)``ce qui va retourner 4.

### Modifier l'HTML avec le JS
On peut accedder à tout les éléments HTML à qui on a donné un identifiant:  
``<p id="cool_p">``   
On peut ensuite modifier l'HTML contenu dans ``<p id="cool_p"> ... </p>`` avec le code JS suivant:  
``document.getElementById("cool_p").innerHTML = bla;``  
Par exemple, imaginons qu'on a un bouton: ``<button onclick="doStuff()"> Do Stuff</button>``  
Alors on pourra modifier notre paragraphe "cool_p" en ajoutant le Script suivant:  
```
	<script>   
		function doStuff() {
			document.getElementById("cool_p").innerHTML = bla;
		}
	</script>
```

Essayez le dans votre propre site !

Aller plus loin
-----
Si vous voulez prendre de l'avance, n'hésitez pas à aller voir le cours en ligne de JS *[W3Schools](https://www.w3schools.com/js/default.asp)*.
