/**
 * @author Andrew Dobis
 */

/**
* @brief Computes the square root of a given value
* @param x, the value for which we want to compute the sqrt
* @return the square root of x
*/
function sqrt(x) {
	let result = 0;
	if(x < 0) {
		alert("Impossible square root");
		return -1;
	} else if(x == 0) {
		return result;
	} else {
		result = Math.pow(x, 1/2);
	}
	return result;
}

function pow(x, num) {
	if(num < 1) {
		alert("pow only works with non");
	}

	let result = 1;
	for (let i = 0; i < num; ++i) {
		result *= x;
	}
	return result;
}

function doMath() {
	let x = prompt("Please enter x", "<insert number>");
	let num = prompt("To what power will you raise x?", "<insert power>");
	alert(x + " to the power of " + num + " is: " + pow(x, num) + "\n and its square root is: " + sqrt(x));
}


