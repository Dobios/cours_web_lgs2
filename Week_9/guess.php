<?php

$found = false;
$expected = rand(0, 9);
	
main();


function main() {
	global $found;
	global $expected;
	$questions = 4;

	echo "Je pense à un nombre entre 0 et 9 \n";

	while(!$found && $questions > 0) {
		$question = readline("Donnes moi une coupure de la forme '{<,>,=}{0,...,9}'\n");
		$found = handleInput($question);
		$questions--;
	}

	echo $found ? "Bravo tu as réussi!\n" : "Meh you failed\n";
	
}

function handleInput($in) {
	global $expected;
	global $found;

	if(preg_match('/<[0-9]/', $in)) {
		$cutOff = substr($in, 1, 1);
		if($cutOff > $expected) {
			echo "nombre < $cutOff\n";
			return false;
		} else {
			echo "nombre >= $cutOff\n";
			return false ;
		}
	} else if(preg_match('/>[0-9]/', $in)) {
		$cutOff = substr($in, 1, 1);
		if($cutOff > $expected) {
			echo "nombre > $cutOff\n";
			return false;
		} else {
			echo "nombre <= $cutOff\n";
			return false;
		}
	} else if(preg_match('/=[0-9]/', $in)) {
		$cutOff = substr($in, 1, 1);
		if($cutOff == $expected) {
			return true;
		} else {
			echo "Nope!\n";
			return false;
		}
	} else {
		echo "Invalid input!!\n";
		return false;
	}
}