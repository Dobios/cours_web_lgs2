<?php declare(strict_types=1);

	function filter_numbers(string $file_name) {
		$in = fopen($file_name, "r") or die("Unable to open file");
		$out = fopen("result_php.txt", "w") or die("Unable to  create result");
		$res = "";

		while (!feof($in)) {
			$char = fgetc($in); //or die("Unable to read character");
			if(!($char >= ('0') && $char <= ('9'))) {
				$res = $res.$char;
			}
		}
		if($res != "") {
			fwrite($out, $res) or die("Unable to write to file");
		}
		
		fclose($in);
		fclose($out);
	}

	filter_numbers("names.txt");